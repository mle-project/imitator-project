#pragma once

#include <memory>
#include <string>

#include <data.h>
#include <event.h>

// Abstract base class for model. Model is an entity that can be fitted to time series and
// predict them.

class Model {
public:
    Model() = default;
    virtual void Fit(std::shared_ptr<Data> data) = 0;

    // Returns predictions for time interval [begin, end] with discretization equals to step
    virtual std::shared_ptr<Data> Predict(const Time& begin, const Time& end, const Time& step) = 0;

    // Return train data
    std::shared_ptr<Data> GetData();

protected:
    std::shared_ptr<Data> data_;
    std::shared_ptr<Data> prediction_;
};