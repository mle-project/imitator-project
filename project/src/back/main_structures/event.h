#pragma once

#include <memory.h>
#include <bits/unique_ptr.h>
#include <variant>

typedef std::variant<int32_t, int64_t, float, double> Time;

// Abstract base class for values of event in time series

class Value {
public:
    virtual ~Value() = 0;
};

struct Event {
    Time time;
    std::unique_ptr<Value> value;
};
