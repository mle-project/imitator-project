#pragma once

#include <vector>

#include <event.h>

// Abstract base class to handle set of events

class Data {
public:
    virtual ~Data() = 0;
};

// Abstract class to handle ordered set of events from one time series

class TimeSeries : public Data {
public:
    virtual ~TimeSeries() override {};

private:
    std::vector<Event> observations_;
};

// Abstract class to handle multiple time series

class SetOfTimeSeries : public Data {
public:
    virtual ~SetOfTimeSeries() override {};

private:
    std::vector<TimeSeries> time_series;
};