#pragma once

#include <memory>

#include <model.h>
#include <data.h>

// A plug that is going to be ExponentialMovingAverage

class ExponentialMovingAverage : public Model {
public:
    ExponentialMovingAverage(double alpha)
            : alpha_(alpha) {};
    virtual void Fit(std::shared_ptr<Data> data) override;
    virtual std::shared_ptr<Data> Predict(const Time& begin, const Time& end, const Time& step) override;
private:
    double alpha_;
};
