#include <exponential_moving_average.h>

void ExponentialMovingAverage::Fit(std::shared_ptr<Data> data) {
    data_ = data;
}

std::shared_ptr<Data> ExponentialMovingAverage::Predict(const Time &begin, const Time &end, const Time &step) {
    return data_;
}