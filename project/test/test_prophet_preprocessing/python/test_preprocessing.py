import pandas as pd
import numpy as np
import json
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from .forecaster import Prophet as ProphetTest

test_kwargs = {key: value for key, value in zip(ProphetTest.__init__.__code__.co_varnames[1:],
                                                ProphetTest.__init__.__defaults__)}

def preprocess_data(data, test_kwargs_change={}, preprocess_kwargs={}):
    test = ProphetTest(**dict(test_kwargs, **test_kwargs_change))
    dat, init = test.fit_like_preprocess(data, **preprocess_kwargs)
    for key in ["y", "t", "t_change", "cap", "s_a", "s_m"]:
        dat[key] = dat[key].tolist()
    dat["X"] = dat["X"].values.tolist()
    for key in ["delta", "beta"]:
        init[key] = init[key].tolist()
    return dat, init

def save_json(data, out_name):
    with open(out_name, "w") as f:
        json.dump(data, f)
        
def load_json(name):
    with open(name, "r") as f:
        data = json.load(f)
    return data
