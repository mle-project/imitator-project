#include <memory.h>

#include <catch.hpp>
#include <exponential_moving_average.h>
#include <data.h>
#include <time.h>

TEST_CASE("Create ema") {
    ExponentialMovingAverage ema(2.5);
}

TEST_CASE("Fit/predict ema") {
    ExponentialMovingAverage ema(2.5);
    std::shared_ptr<Data> data;
    ema.Fit(data);
    Time t;
    data = ema.Predict(t, t, t);
}

