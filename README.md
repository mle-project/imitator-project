# imitator-project

## Полезные материалы

### Основные

* https://peerj.com/preprints/3190.pdf
* https://github.com/facebook/prophet
* https://github.com/stan-dev/stan

### Статейки

* ...

### Прочее

* https://plot.ly
* https://plot.ly/products/dash/
* http://www.statsmodels.org/dev/tsa.html#